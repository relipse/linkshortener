<?php
/**
 * Copyright 2021 James A Kinsman
 */
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Link;

class GoNowController extends AbstractController
{
    // ...

    /**
     * @Route("/{slug}", name="gotourl")
     */
    public function go(string $slug): Response
    {
        $entitymanager = $this->getDoctrine()->getManager();
        $link = $entitymanager->getRepository(Link::class)->findOneBy(['slug'=>$slug]);
        if (!$link){
            throw $this->createNotFoundException('Link does not exist');
        }
        $link->setHits($link->getHits() + 1);
        $entitymanager->flush();
        return $this->redirect($link->getFullUrl());
        //return $this->render('view/view.html.twig', ['slug' => $slug]);
    }
}
<?php
/**
 * Copyright 2021 James A Kinsman
 */
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Link;

class FavoritesController extends AbstractController
{
    /**
     * @Route("/favorites", name="favorites", methods={"GET"})
     */
    public function main(): Response
    {
        $entitymanager = $this->getDoctrine()->getManager();
        $linkrepo = $entitymanager->getRepository(Link::class);
        $favorites = $linkrepo->findBy([], ['hits' => 'DESC', 'full_url' => 'ASC']);
        return $this->render('main/favorites.html.twig', ['favorites' => $favorites]);
    }
}
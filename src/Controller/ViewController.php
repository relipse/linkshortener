<?php
/**
 * Copyright 2021 James A Kinsman
 */
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Link;

class ViewController extends AbstractController
{

    /**
     * @Route("/view/{slug}", name="link_shortener")
     */
    public function view(string $slug): Response
    {
        $entitymanager = $this->getDoctrine()->getManager();
        $link = $entitymanager->getRepository(Link::class)->findOneBy(['slug'=>$slug]);
        if (!$link){
            throw $this->createNotFoundException('Link does not exist');
        }
        return $this->render('view/view.html.twig', ['slug' => $slug, 'full_url'=>$link->getFullUrl(), 'hits'=>$link->getHits()]);
    }
}
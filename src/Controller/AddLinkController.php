<?php
/**
 * Copyright 2021 James A Kinsman
 */
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Link;
use Symfony\Component\HttpFoundation\Request;


class AddLinkController extends AbstractController
{
    /**
     * @Route("/", name="main", methods={"GET"})
     */
    public function main(): Response
    {
        return $this->render('main/addlink.html.twig');
    }

    /**
     * @Route("/", methods={"POST"})
     */
    public function add(): Response
    {
        $request = Request::createFromGlobals();
        $full_url = $request->request->get('full_url');
        $parsed = parse_url($full_url);
        if (empty($parsed['scheme']) || empty($parsed['host'])){
            return $this->render('main/addlink.html.twig',['error'=>'Invalid url']);
        }
        if ($parsed['scheme'] != 'http' && $parsed['scheme'] != 'https'){
            return $this->render('main/addlink.html.twig',['error'=>'Invalid url. Must be http:// or https://']);
        }
        // If we got here, then it is a valid url
        $entitymanager = $this->getDoctrine()->getManager();
        $linkrepo = $entitymanager->getRepository(Link::class);

        $count = 0;
        $slug = $this->generateSlug();
        while($linkrepo->findOneBy(['slug'=>$slug])){
            $count++;
            if ($count > 10){
                return $this->render('main/addlink.html.twig',['error'=>'Server error: No more slugs available']);
            }
            $slug = $this->generateSlug();
        }
        //Ok, now we have a unique slug
        $link = new Link();
        $link->setFullUrl($full_url);
        $link->setSlug($slug);
        $link->setHits(0);

        $entitymanager->persist($link);

        $entitymanager->flush();

        return $this->redirectToRoute('link_shortener', ['slug'=>$slug]);
    }


    /**
     * Generate random slug (idea from https://stackoverflow.com/questions/4356289/php-random-string-generator )
     * @return string
     */
    protected function generateSlug() : string{
        $length = rand(5,9);
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
Developed in WSL2 (Ubuntu 20.04)

composer require doctrine/annotations

composer require twig

composer require symfony/apache-pack

Using sqlite (sudo apt-get install php7.4-sqlite)
I used ```php bin/console make:entity```
To auto-create the src/Entity/Link.php file
Then ```php bin/console make:migration```
Then ```php bin/console doctrine:migrations:migrate```

Which creates a table similar to this:
CREATE TABLE "link" (
"id"	INTEGER NOT NULL,
"slug"	TEXT NOT NULL UNIQUE,
"full_url"	TEXT NOT NULL,
"hits"	INTEGER DEFAULT 0,
PRIMARY KEY("id" AUTOINCREMENT)
);

Logo generated from https://cooltext.com/Logo-Design-Comic

Also, I had to install yarn and run yarn install, and make sure my node version 
was up to date, I had 10.19.0 so I needed to do apt-get remove nodejs

And install nvm https://github.com/nvm-sh/nvm to manager node installs

Then finally ```nvm install node```

Then in root dir ```yarn install```
and ```yarn build```


At first, I was using apache to serve, but then switched to ```symfony server:start``` to fix the assets

Need to run ```yarn build``` each time changing css.

Development Thoughts:
Starting out with 0-knowledge about Symfony and trying to get done within a day was a challenge.
However, I dug into the documentation and asked some questions in the Symfony Developer slack channel and finally got 
my dev environment working with yarn for the front end. Once I got the hang of Symfony I really enjoy the development in it, but am
looking forward to your offer of a Symfony Certification!

I decided to implement a couple of the extra features:
 - Analytics (Hits per slug)
 - Favorites

I did not do much work on the front end, just a little (added logo and some css), 
This is mostly due to time constraints. I chose to use sqlite for the database (simple and effective).